#!/usr/bin/env pythoh3

'''
Program to produce a triangle with numbers
'''

import sys


def line(number: int):
    """Return a string corresponding to the line for number"""
    string = str(number)
    linea = string * number
    linea = linea.strip("\n")
    return linea


def triangle(number: int):
    """Return a string corresponding to the triangle for number"""
    pyramid = ""
    if number > 9:
        raise ValueError("El valor debe ser menor de 9")
    else:
        for i in range(1, number+1):
            pyramid += line(i) + "\n"
            pyramid=pyramid.lstrip("\n")
        return pyramid


def main():
    number: int = sys.argv[1]
    text = triangle(int(number))
    print(text)


if __name__ == '__main__':
    main()